/**
  * @author Yuliang.Lee
  * @date 2021/9/25 17:47
  * @version 1.0
  * 统计数组中出现的单词次数（Scala特有的函数式编程，能大大简化开发工作）
  */
object WordCount {
  def main(args: Array[String]): Unit = {
    val arr1 = Array(1,2,3,4,5)
    // 内置的高阶函数map，示例arrl.map(f)：
    // 每次都从arr1拿出一个元素给f作为参数执行计算，map方法的返回结果就是每次f执行完之后那些结果的集合
    arr1.map((x:Int) => x*2)    //返回Array(2,4,6,8,10)
    // filter举例，逻辑体的返回值必须是布尔类型
    arr1.filter(x => if (x > 3) true else false)   //返回Array(4,5)

    val array = Array("hello aa", "hello bb", "hello cc", "aa bb aa")
    // 根据空格拆分每一行的单词
    array.map(line => line.split(" "))          //返回Array(Array("hello","aa"), Array("hello", "bb"), ...)
    // 将上一步的数组扁平化
    array.map(line => line.split(" ")).flatten  //返回Array("hello","aa","hello","bb"...)
    // 将扁平后的数组元素进行分组
    array.map(line => line.split(" ")).flatten.groupBy(x => x)  //返回Map("bb" -> Array("bb","bb"), "aa" -> Array("aa","aa","aa")...)
    // 对分组后的map进行统计
    val res = array.map(line => line.split(" ")).flatten.groupBy(word => word).map(x => (x._1, x._2.length))
    // 直接打印，返回Map(bb -> 2, cc -> 1, hello -> 3, aa -> 3)
    println(res)
    // 遍历打印
    for (e <- res) println(e)

    // 对res进行排序
    res.toList.sortBy(x => x._2)
    // 根据出现次数降序排序
    res.toList.sortBy(x => x._2).reverse


  }
}
