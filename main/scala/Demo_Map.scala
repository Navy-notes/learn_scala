import scala.collection.mutable

/**
  * @author Yuliang.Lee
  * @date 2021/9/25 15:28
  * @version 1.0
  */
object Demo_Map {
  def main(args: Array[String]): Unit = {
//    val map1 = new mutable.HashMap[String, Int]()
//    // 向map中添加数据
//    map1("spark") = 1
//    map1 += (("hadoop", 2))
//    map1.put("storm", 3)
//    println(map1)
//
//    // 从map中移除元素
//    map1 -= "spark"
//    map1.remove("hadoop")
//    println(map1)

    // 初始化定义一个map
    val m1 = Map("tom" -> 80, "jerry" -> 90, "kitty" -> 85)
    val m2 = Map(("tom",80), ("jerrt", 90), ("kitty", 85))
    // 获取值
    println(m1("tom"))                // 80
    println(m1("who"))                // 会抛异常
    println(m1.get("tom"))            // Some(80)
    println(m1.get("who"))            // None
    println(m1.getOrElse("tom", 60))  //80
    println(m1.getOrElse("who", 60))  //60

    // map的遍历
    for (e <- m1) {
      println(e) // 等价于println(e._1, e._2)
    }
    for (e <- m1) {
      println(e._1)
    }
    for (e <- m1) {
      println(e._1 + ":" + e._2)
    }
  }
}
