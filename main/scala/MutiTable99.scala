/**
  * @author Yuliang.Lee
  * @date 2021/9/22 23:17
  * @version 1.0
  * 99乘法表
  */
object MutiTable99 {
  def main(args: Array[String]): Unit = {
    // java风格
//    for (i <- 1 to 9) {
//      var flag = "\t"
//      for (j <- 1 to i) {
//        if (j == i) flag = "\n"
//        print(i + "*" + j + "=" + i*j + flag)
//      }
//    }

    //scala风格
    for(i <- 1 to 9; j <- 1 to i) print(i + "*" + j + "=" + i*j + (if(i == j) "\n" else "\t"))

  }
}
