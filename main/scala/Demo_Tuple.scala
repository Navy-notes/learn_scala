/**
  * @author Yuliang.Lee
  * @date 2021/9/25 15:39
  * @version 1.0
  */
object Demo_Tuple {
  def main(args: Array[String]): Unit = {
    val (a,b,c), t = ("huangbo", 18, true)
    println(a)
    println(b)
    println(c)
    println(t)
    // 取元组的元素下标从1开始，并且元素的引用不可改，但是引用的值可以变化
    println(t._1)
    println(t._2)
    println(t._3)

    // 二元组数组转map
    val a1 = Array(("a", 11), ("b", 12))
    println(a1.toMap)

    // 元组的拉链操作
    val names = Array("tom", "jerry", "kitty")
    val scores = Array(88, 95 ,90)
    val res = names.zip(scores).toMap
    println(res)

  }
}
