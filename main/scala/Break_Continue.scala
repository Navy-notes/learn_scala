import util.control.Breaks._

/**
  * @author Yuliang.Lee
  * @date 2021/9/22 22:42
  * @version 1.0
  */
object Break_Continue {

  def main(args: Array[String]): Unit = {
    // break举例  输出：1 2
//    breakable(
//      for (i <- 1 to 5) {
//        if (i == 3) {
//          break()
//        }
//        println(i)
//      }
//    )

    // continue举例 输出：1 2 4 5
    for (i <- 1 to 5) {
      breakable {
        if (i == 3) {
          break()
        }
        println(i)
      }
    }

  }
}
