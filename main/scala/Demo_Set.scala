import scala.collection.mutable

/**
  * @author Yuliang.Lee
  * @date 2021/9/25 15:22
  * @version 1.0
  */
object Demo_Set {
  def main(args: Array[String]): Unit = {
    val set1 = Set(1,2,3,4,5)
    val set2 = Set(3,4,5,6,7)
    println(set1 union set2)		   //返回Set(5,1,6,2,7,3,4)
    println(set1 diff set2)		    //返回Set(1,2)
    println(set2 diff set1)		    //返回Set(6,7)
    println(set1 intersect set2)	//返回Set(5,3,4)
  }

}
