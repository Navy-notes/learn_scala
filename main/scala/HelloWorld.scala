/**
  * @author Yuliang.Lee
  * @date 2021/9/21 0:07
  * @version 1.0
  */
object HelloWorld {
  /**
    * def   用来定义方法的关键字
    * main  方法名
    * args  形参名称
    * Array[String] args这个形参的数据类型，是String类型的数组，相当于java中的 String[]
    * Unit  方法的返回值，相当于java中的 void
    *
    * 例：def max(a: Int, b: Int): Int = {}
    * @param args
    */
  def main(args: Array[String]): Unit = {
    println("hello scala!")
  }



}
