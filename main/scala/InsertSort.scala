import scala.collection.mutable.{ArrayBuffer, ListBuffer}

/**
  * @author Yuliang.Lee
  * @date 2021/9/25 19:39
  * @version 1.0
  */
object InsertSort {
  def main(args: Array[String]): Unit = {
    // insert的用法
    val arr = ArrayBuffer(1,2,3,4)
    // 在下标为2的元素前面加入7
    arr.insert(2, 7)              //ArrayBuffer(1,2,7,3,4)
    // 在下标为2的元素前面加入多个元素
    arr.insert(2, 8, 9, 10)       //ArrayBuffer(1, 2, 8, 9, 10, 7, 3, 4)
    println(arr)

    // 插入排序
    for (e <- isort(arr.toList)) {
      print(e + " ")
    }

  }

  def isort(xs: List[Int]): List[Int] = {
    if (xs.isEmpty) Nil
    else  insert(xs.head, isort(xs.tail))
  }

  def insert(x: Int, xs: List[Int]): List[Int] = {
    if (xs.isEmpty || x <= xs.head) x :: xs
    else  xs.head :: insert(x, xs.tail)
  }
}
